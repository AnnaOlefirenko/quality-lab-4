#include "metric1.h"

metric1::metric1(QObject *parent) : QObject(parent)
{
    file = new QFile("D:\\a.cpp");
}

float metric1::AverageNumCodeStr()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
        {
            QTextStream in(file);
            while (!in.atEnd())
            {
                QString code = in.readAll();
                QStringList list=code.split(QRegExp("(void|int|string|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
                    int s=0;
                    for(int i=list.count()-1;i>=0;--i)
                    {
                        const QString& item=list[i];
                        s=s+item.count("\n");
                    }
                    float res=(float)s/(list.count())-1;
                    file->close();
                    return res;
            }
    }
}

float metric1::NumMethods()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList list=code.split(QRegExp("(void|int|string|float|double|byte)[A-Za-z0-9]*"),QString::SkipEmptyParts);
                float sl=list.count()-1;
                file->close();
                return sl;
        }
    }
}

float metric1::NumModulsInclude()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList lst=code.split(QRegExp("#include"),QString::SkipEmptyParts);
            float sl=lst.count()-1;
            file->close();
            return sl;
        }
    }
}

float metric1::NumClasses()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList lst=code.split(QRegExp("class"),QString::SkipEmptyParts);
            float sl=lst.count()-1;
            file->close();
            return sl;

        }
    }
}

float metric1::AverageNumClassStr()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList lst=code.split(QRegExp("class"),QString::SkipEmptyParts);
            int s=0;
            for(int i=lst.count()-1;i>=0;--i)
            {
                const QString&item=lst[i];
                s=s+item.count("\n")-1;
            }
            float sl=(float)s/(lst.count());
            file->close();
            return sl;
        }
    }
}

int metric1::NORM()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList lst=code.split(QRegExp("class"),QString::SkipEmptyParts);
            int s=0;
            for(int i=lst.count()-1;i>=0;--i)
            {
                const QString&item=lst[i];
                QStringList list=item.split("){",QString::SkipEmptyParts);
                s += item.count("this->");
            }
            file->close();
            return s;
        }
    }
}

int metric1::RFC()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList lst=code.split(QRegExp("class"),QString::SkipEmptyParts);
            int s=0;
            for(int i=lst.count()-1;i>=0;--i)
            {
             const QString&item=lst[i];
             QStringList list=item.split("){",QString::SkipEmptyParts);
             s=s+item.count(")")-item.count(")");
            }
            file->close();
            return s;
        }
    }
}

float metric1::MWC()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList lst=code.split(QRegExp("class"),QString::SkipEmptyParts);
            int s=0;
            for(int i=lst.count()-1;i>=0;--i)
            {
                const QString&item=lst[i];
                QStringList list=item.split("){",QString::SkipEmptyParts);
                s=0;
                for(int j=list.count()-1;j>=0;--j)
                {
                    const QString&ite=list[j];
                    if((ite.count("{")>=ite.count("}"))&&(ite.count("}")!=0))
                        s=s+1;
                }
            }
            file->close();
            return s;
        }
    }
}

float metric1::NOC()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString code = in.readAll();
            QStringList lst=code.split(QRegExp("class"),QString::SkipEmptyParts);
                QStringList lis,Lclss,Rclss;
                for(int g=0;g<lst.count();g++)
                {
                    const QString&item=lst[g].trimmed();
                    QString St="";
                    for(int i=0;i<item.count();i++)
                    if (item[i]!='\n')
                        St=St+QString(item[i]);
                    else
                        break;
                    lis<<St;
                }
                for(int g=0;g<lis.count();g++)
                {
                    const QString&item=lis[g].trimmed();
                    QString St="";
                    for(int i=0;i<item.count();i++)
                    if(item[i]!=' ')
                        St=St+QString(item[i]);
                    else
                        break;
                    Rclss<<St;
                }
                for(int g=0;g<lis.count();g++)
                {
                    const QString&item=lis[g].trimmed();
                    QString St="";
                    lst=item.split(QRegExp("public|protected|private"),QString::SkipEmptyParts);
                    for(int i=0;i<lst.count();i++)
                    {
                        St=St+lst[i];
                    }
                    Lclss<<St;
                }
                for(int g=0;g<Rclss.count();g++)
                {
                    Lclss[g].replace(0,Rclss[g].count(),"");
                    Lclss[g]=Lclss[g].trimmed();
                }
                int s=0;
                for(int g=0;g<Rclss.count();g++)
                {
                    Rclss[g].remove(":");
                    for(int i=0;i<Rclss.count();i++)
                    {
                        s=s+Lclss[i].count(Rclss[g]);
                    }
                }
                file->close();
                return s;
        }
    }
}

